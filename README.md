# My Portfolio

![Build Status](https://gitlab.com/GordonClanProgramming/gordonclanprogramming.gitlab.io/badges/master/pipeline.svg)

---

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---
<!-- npm install -g doctoc; then run: doctoc README.md -->
<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [ToDo](#todo)
- [Links](#links)
- [Icons](#icons)
- [Inspiration](#inspiration)
- [GitLab CI](#gitlab-ci)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## ToDo

* contact info
* add projects to projects section
    git
* include reccomendation quotes from former coworkers
* dark/light mode?

## Links

<https://evangordon.dev/>

[Pipeline](https://gitlab.com/GordonClanProgramming/gordonclanprogramming.gitlab.io/-/pipelines)

[Domain Registered With Hover](https://www.hover.com)

## Icons

[SVG Icons](https://github.com/simple-icons/simple-icons/tree/develop/icons)

[SVG Dev Icons](https://devicon.dev/)

## Inspiration

[Building a great portfolio](https://medium.com/actiresults/how-to-build-a-great-developer-portfolio-examples-tools-db3cf3d97531)

<https://iuri.is/work>

<http://riccardozanutta.com/>

<https://prashantsani.com/>

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: alpine:latest

pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
```

The above example expects to put all your HTML files in the `public/` directory.

## Troubleshooting

1. CSS is missing! That means that you have wrongly set up the CSS URL in your
   HTML files. Have a look at the [index.html] for an example.

[ci]: https://about.gitlab.com/gitlab-ci/
[index.html]: https://gitlab.com/pages/plain-html/blob/master/public/index.html
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
